helm install -f values/redis-values.yaml rediscart charts/redischart

helm install -f values/email-service-values.yaml emailservice charts/webappchart
helm install -f values/ad-service-values.yaml adservice charts/webappchart
helm install -f values/cart-service-values.yaml cartservice charts/webappchart
helm install -f values/checkout-service-values.yaml checkoutservice charts/webappchart
helm install -f values/currency-service-values.yaml currencyservice charts/webappchart
helm install -f values/frontend-service-values.yaml frontendservice charts/webappchart
helm install -f values/payment-service-values.yaml paymentservice charts/webappchart
helm install -f values/productcatalog-service-values.yaml productservice charts/webappchart
helm install -f values/shipping-service-values.yaml shippingservice charts/webappchart




